import pygame
import random

pygame.init()

WIDTH, HEIGHT = 800, 600

# Colors

YELLOW = (239, 183, 0)
GREEN = (124, 252, 0)
RED = (184, 29, 19)

# Grid properties
GRID_ROWS = 6
GRID_COLS = 5
CELL_WIDTH = WIDTH // GRID_COLS
CELL_HEIGHT = HEIGHT // GRID_ROWS

word_list = []

with open("words") as my_words:
    for line in my_words:
        word_list.append(line.strip())

# print(word_list) 

current_guess = []
current_row = 0
current_colors = []

all_guesses = []
all_colors = []

secret_word = word_list[random.randint(0, 3318)]
print(secret_word)

screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("PyWordle")

def draw_grid():
    for row in range(GRID_ROWS):
        for col in range(GRID_COLS):
            rect_x = col * CELL_WIDTH
            rect_y = row * CELL_HEIGHT
            pygame.draw.rect(screen, (0, 51, 79), (rect_x, rect_y, CELL_WIDTH, CELL_HEIGHT), 1)
            
            # Draw previous
            if row < len(all_guesses) and col < len(all_guesses[row]):
                font = pygame.font.SysFont('Arial', 30)
                letter_image = font.render(all_guesses[row][col], True, all_colors[row][col])
                screen.blit(letter_image, (rect_x + CELL_WIDTH // 4, rect_y + CELL_HEIGHT // 4))
    
            # Draw current
            if row == current_row and col < len(current_guess):
                font = pygame.font.SysFont('Arial', 30)
                letter_image = font.render(current_guess[col], True, (0, 0, 0))
                screen.blit(letter_image, (rect_x + CELL_WIDTH // 4, rect_y + CELL_HEIGHT //4))

def _popup(message):
    popup_width = 500
    popup_height = 300
    popup_x = (WIDTH - popup_width) // 2
    popup_y = (HEIGHT - popup_height) // 2

    pygame.draw.rect(screen, (200, 200, 200), (popup_x, popup_y, popup_width, popup_height))

    font = pygame.font.SysFont('Arial', 30)
    text_image = font.render(message, True, (0, 0, 0))
    text_rect = text_image.get_rect(center=(WIDTH // 2, HEIGHT // 2))

    screen.blit(text_image, text_rect)

#def show_title():
    #font = pygame.font.SysFont('Arial', 50, bold=True)
    #title_image = font.render("PyWordle", True, (0, 0, 0))
    #screen.blit(title_image, (WIDTH // 2 - title_image.get_width() // 2, 10))


running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        
        if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    if len(current_guess) == 5:
                        if ''.join(current_guess) == secret_word:
                            for x in range(len(current_guess)):
                                current_colors[x] = GREEN
                            _popup("Well done! word was:" + ''.join(current_guess))
                            pygame.display.flip()
                            pygame.time.wait(2000)

                        else: 
                            for x in range(len(current_guess)):                                
                                if current_guess[x] == secret_word[x]:
                                    current_colors[x] = GREEN

                                elif current_guess[x] in secret_word:
                                    current_colors[x] = YELLOW

                                else:
                                    current_colors[x] = RED

                            all_guesses.append(current_guess)
                            all_colors.append(current_colors)

                            current_row += 1
                            current_guess = []
                            current_colors = []

                elif event.key == pygame.K_BACKSPACE:
                    print("backspace")
                    if current_guess:
                        current_guess.pop()
                        current_colors.pop()

                else:
                    letter = pygame.key.name(event.key)
                    if len(letter) == 1 and letter.isalpha() and len(current_guess) < 5:
                        current_guess.append(letter)
                        current_colors.append((0, 0, 0))
                        print(current_guess)

                if event.key == pygame.K_0:
                    current_guess = []
                    current_colors = []
                    current_row = 0
                    secret_word = word_list[random.randint(0, 3318)]

        if current_row == 6:
            _popup("Game Over! Word was: " + secret_word)
            pygame.display.flip()
            pygame.time.wait(2000)
            running = False


    screen.fill((90, 90, 90)) 

    draw_grid()

    pygame.display.flip()

pygame.quit()
